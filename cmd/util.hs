import Control.Monad {- base -}
import Data.List {- base -}
import System.Environment {- base -}
import System.FilePath {- filepath -}
import System.IO {- base -}
import Text.Printf {- base -}

import qualified Data.Array.Unboxed as A {- array -}
import qualified Numeric.LinearAlgebra as G {- hmatrix -}

import qualified Music.Theory.Array.Csv as T {- hmt-base -}
import qualified Music.Theory.Read as T {- hmt-base -}

import qualified Sound.Sc3.Plot as P {- hsc3-plot -}

import qualified Sound.Analysis.Meapsoft as M {- hmeap -}

-- * Array, Matrix

m_columns :: G.Element t => G.Matrix t -> [Int] -> [G.Vector t]
m_columns mx ns = let c = G.toColumns mx in map (c !!) ns

fromArray :: A.UArray (Int, Int) Double -> G.Matrix Double
fromArray m =
  let ((r0, c0), (r1, c1)) = A.bounds m
      r = r1 - r0 + 1
      c = c1 - c0 + 1
  in (r G.>< c) (A.elems m)

-- * Browse

user_interact :: M.Meap Float -> IO ()
user_interact m = do
  putStr "feature> "
  i <- fmap (T.read_def 0) getLine
  putStr "frame> "
  j <- fmap (T.read_def 0) getLine
  let f = M.features m !! i
      l = M.feature_column f
      r = l + M.feature_degree f - 1
  putStrLn (show ("feature", f))
  putStrLn (show ("chunk", map (\c -> M.position m (j, c)) [l .. r]))

browse :: FilePath -> IO ()
browse fn = do
  Right m <- M.read_meap fn
  putStrLn
    ( show
        ( "nframes"
        , M.n_frames m
        , "nfeatures"
        , length (M.features m)
        )
    )
  forever (user_interact m)

-- * Convert

read_double :: String -> Double
read_double = maybe (error "read_double") id . T.read_maybe

convert_onsets :: FilePath -> IO ()
convert_onsets fn = do
  s <- readFile fn
  let w :: [String]
      w = words s
      n :: [Double]
      n = map read_double w
      t :: [(Double, Double)]
      t = zipWith (\p q -> (p, q - p)) n (tail n)
      fn' = dropExtension fn
      fmt :: (Double, Double) -> String
      fmt (p, q) = printf "%s\t%f\t%f" fn' p q
  putStrLn "#filename\tonset_time\tchunk_length"
  mapM_ (putStrLn . fmt) t

-- * Plot

normalize :: G.Vector Double -> G.Vector Double
normalize v =
  let l = G.minElement v
      r = G.maxElement v
      d = r - l
      v' = G.cmap (+ (negate l)) v
  in G.scale (recip d) v'

to_matrix :: M.Meap Double -> G.Matrix Double
to_matrix mp = fromArray (M.uarray_data mp)

-- | Columns indices given feature names.
mp_columns :: M.Meap t -> [String] -> [Int]
mp_columns mp ns =
  let fs = M.features mp
      uf = map (\n -> M.required_feature n fs) ("onset_time" : ns)
  in map M.feature_column uf

plot :: FilePath -> [String] -> IO ()
plot fn ns = do
  (Right mp) <- M.read_meap fn
  let mx = to_matrix mp
      vs = m_columns mx (mp_columns mp ns)
      vs' = map normalize vs
      (ot : ls) = map G.toList vs'
      ls' = map (zip ot) ls
  P.plot_p2_ln ls'

-- * Print header

-- > let fn = "/home/rohan/cvs/tn/tn-55/meapsoft/data/chris_mann.wav.feat"
-- > print_header fn
print_header :: FilePath -> IO ()
print_header fn = do
  Right h <- M.read_header fn
  let hdr = ["Feature Name", "Column", "Degree"]
      gen_row (M.Feature n c d) = [n, show c, show d]
      opt = (False, ',', False, T.Csv_Align_Right)
      csv = T.csv_table_pp id opt (Just hdr, map gen_row h)
  putStrLn csv

-- * Segment

type Segment = (Double, Double)

load_seg :: FilePath -> IO [Segment]
load_seg fn = do
  Right m <- M.read_meap fn
  return (M.segments_l m)

write_seg :: FilePath -> [Segment] -> IO ()
write_seg nm sg =
  let fmt (s, d) = show s ++ "," ++ show d
  in writeFile nm (unlines (map fmt sg))

-- * Stat

-- | (feature,minima,maxima,norm2,avg)
type Stat = (String, (Double, Int), (Double, Int), (Double, Double), Double)

v_abs_sum :: G.Vector Double -> Double
v_abs_sum = G.sumElements . G.cmap abs

mp_stat :: M.Meap Double -> [Stat]
mp_stat mp =
  let nf = fromIntegral (M.n_frames mp)
      uf = filter ((== 1) . M.feature_degree) (M.features mp)
      ar = M.uarray_data mp
      m = fromArray ar
      ufc = map M.feature_column uf
      vs = m_columns m ufc
  in zip5
      (map M.feature_name uf)
      (zip (map G.minElement vs) (map G.minIndex vs))
      (zip (map G.maxElement vs) (map G.maxIndex vs))
      (zip (map v_abs_sum vs) (map G.norm_2 vs))
      (map ((/ nf) . v_abs_sum) vs)

-- > let fn = "/home/rohan/cvs/tn/tn-55/meapsoft/data/chris_mann.wav.feat"
-- > stat fn
stat :: FilePath -> IO [Stat]
stat fn = do
  (Right mp) <- M.read_meap fn
  return (mp_stat mp)

-- * Main

help :: [String]
help =
  [ "hmeap-util cmd arg"
  , "  browse feature-file"
  , "  convert onsets input-file"
  , "  plot feature-file feature..."
  , "  print-header analysis-file"
  , "  segments feature-file csv-file"
  , "  stat feature-file"
  , ""
  , "    features: " ++ unwords M.feature_names
  ]

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  a <- getArgs
  case a of
    ["browse", fn] -> browse fn
    ["convert", "onsets", fn] -> convert_onsets fn
    "plot" : fn : ns -> plot fn ns
    ["print-header", fn] -> print_header fn
    ["segment", i_fn, o_fn] -> load_seg i_fn >>= write_seg o_fn
    ["stat", fn] -> stat fn >>= mapM_ (putStrLn . show)
    _ -> putStrLn (unlines help)

{-
> let fn = "/home/rohan/disk/saikaku/archive/cvs/tn/tn-55/meapsoft/data/chris_mann.wav.feat"
> plot fn ["AvgChroma"]
-}
