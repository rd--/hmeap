import Control.Monad {- base -}
import Data.List {- base -}
import System.Directory {- directory -}
import System.Environment {- base -}
import System.Exit {- base -}
import System.FilePath {- filepath -}
import System.IO {- base -}
import Text.Printf {- base -}
import Text.Read {- base -}

import qualified Data.IntSet as S {- containers -}
import qualified Text.CSV.Lazy.String as C {- lazy-csv -}

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

import Sound.Analysis.Meapsoft as M {- hmeap -}

sin_env :: Ugen -> Ugen -> Ugen
sin_env dur amp =
  let d = envSine dur amp
  in envGen ar 1 1 0 1 RemoveSynth d

rect_env :: Ugen -> Ugen -> Ugen
rect_env dur amp =
  let d = envLinen 0.01 dur 0.01 amp
  in envGen ar 1 1 0 1 RemoveSynth d

-- | Play interval from sound file.
mk_iseg :: String -> (Ugen -> Ugen -> Ugen) -> Synthdef
mk_iseg nm e =
  let k = control kr
      b = k "bufnum" 0 -- buffer number
      s = k "start" 0 -- start time (seconds)
      d = k "sustain" 1 -- duration (seconds)
      r = k "rate" 1 -- playback rate
      a = k "amp" 0.2 -- linear amplitude scalar
      p = k "pan" 0.0
      rs = bufRateScale kr b * r
      -- see adc for rationale
      ph = phasor ar 0 rs 0 (d * sampleRate) 0 + (s * sampleRate)
      o = pan2 (bufRdL 1 ar b ph Loop * e d a) p 1
  in synthdef nm (offsetOut 0 o)

isegS :: Synthdef
isegS = mk_iseg "isegS" sin_env

isegR :: Synthdef
isegR = mk_iseg "isegR" rect_env

type Segment = (Double, Double)

-- > let fn = "/home/rohan/data/audio/bailey/after-five-weeks.T0.5.seg"
-- > sg <- load_seg fn
-- > map (sg !!) [0..9]
load_seg :: FilePath -> IO [Segment]
load_seg fn = do
  Right m <- read_meap fn
  return (segments_l m)

load_csv :: FilePath -> IO [Segment]
load_csv fn = do
  s <- readFile fn
  let t = C.csvTable (C.parseCSV s)
      p = C.fromCSVTable t
      f e = case e of
        [st, du] -> (read st, read du)
        _ -> error "load_csv"
  return (map f p)

spl :: String -> Segment -> Double -> Message
spl nm (s, d) a =
  let p = [("bufnum", 10), ("start", s), ("sustain", d), ("amp", a), ("pan", 0)]
  in s_new nm (-1) AddToTail 1 p

type Annotated_Segment = (Int, Segment, Bool)

data State = State
  { au_fn :: FilePath
  , au_id :: Int
  , sg_csv :: Bool
  , sg_fn :: FilePath
  , sg_data :: [Annotated_Segment]
  , sg_size :: Int
  , sg_loc :: Int
  , sg_mark :: S.IntSet
  , iseg_nm :: String
  , hr_iot :: Double {- seconds -}
  , hr_amp :: Double {- linear gain -}
  }

init_st :: Bool -> FilePath -> FilePath -> State
init_st csv au sg =
  State
    { au_fn = au
    , au_id = 10
    , sg_csv = csv
    , sg_fn = sg
    , sg_data = []
    , sg_size = 0
    , sg_loc = 0
    , sg_mark = S.empty
    , iseg_nm = "isegR"
    , hr_iot = 0
    , hr_amp = 1
    }

st_loader :: State -> IO State
st_loader st = do
  sg <- (if sg_csv st then load_csv else load_seg) (sg_fn st)
  withSc3
    ( do
        _ <- async (b_allocReadChannel (au_id st) (au_fn st) 0 0 [0])
        _ <- async (d_recv isegS)
        _ <- async (d_recv isegR)
        return ()
    )
  return
    ( st
        { sg_data = zip3 [0 ..] sg (repeat True)
        , sg_size = length sg
        }
    )

-- > map (\(c,_,_) -> c) cmd_help
cmd_help :: [(Char, String, String)]
cmd_help =
  [ ('?', "help", "print command reference")
  , -- navigation
    ('g', "goto <segment>", "move to indicated segment")
  , ('f', "forward", "move forward one segment")
  , ('b', "back", "previous segment")
  , ('F', "fast-forward", "move forward ten segments")
  , ('B', "fast-backward", "move backward ten segments")
  , ('z', "zero", "move to segment zero")
  , -- annotation
    ('m', "mark", "add current segment to mark list")
  , ('u', "unmark", "remove current segment from mark list")
  , ('M', "merge <marks>", "merge in mark list")
  , -- filtration
    ('>', "> <duration>", "do not consider segments less than duration")
  , ('<', "< <duration>", "do not consider segments greater than duration")
  , -- printing
    ('i', "information", "print segmentation information")
  , ('d', "data", "print segmentation data")
  , ('l', "list-indices", "print mark index list")
  , ('L', "list-segments", "print mark segment list")
  , -- hearing
    ('p', "play", "play current segment")
  , ('n', "next", "play,forward")
  , ('t', "timing <duration>", "delay between sequenced segments (seconds)")
  , ('h', "hear-marked", "play marked segments in sequence")
  , ('H', "hear-all", "play all segments in sequence")
  , ('a', "accept", "mark,forward,play")
  , ('r', "reject", "unmark,forward,play")
  , ('v', "volume <real>", "set playback gain (linear)")
  , ('w', "window <type>", "set segment playback window {sin|rect}")
  , -- ending
    ('q', "quit", "system exit")
  ]

st_current_annotated_segment :: State -> Annotated_Segment
st_current_annotated_segment st = sg_data st !! sg_loc st

st_current_segment :: State -> Segment
st_current_segment st = p3_snd (sg_data st !! sg_loc st)

st_set_loc :: State -> Int -> State
st_set_loc st n =
  let u = sg_size st - 1
      r = if n < 0 then 0 else if n > u then u else n
  in st {sg_loc = r}

st_relocate :: State -> Int -> State
st_relocate st n =
  let (k, _, _) = st_current_annotated_segment st
      st' = st_set_loc st (sg_loc st + n)
      (k', _, p') = st_current_annotated_segment st'
  in if not p' && k /= k'
      then st_relocate st' n
      else st'

st_filter_dur :: State -> Ordering -> Double -> State
st_filter_dur st cmp n =
  let f (k, (s, d), p) =
        if compare n d == cmp
          then (k, (s, d), False)
          else (k, (s, d), p)
  in st {sg_data = map f (sg_data st)}

p3_snd :: (a, b, c) -> b
p3_snd (_, b, _) = b

st_play_cur :: State -> IO State
st_play_cur st = do
  let (s, d) = st_current_segment st
  withSc3 (sendMessage (spl (iseg_nm st) (s, d) (hr_amp st)))
  pauseThread d
  return st

play_seq :: String -> Double -> [Annotated_Segment] -> Double -> IO ()
play_seq nm dt sg a = do
  withSc3
    ( mapM_
        ( \(k, (s, d), p) -> do
            liftIO (print ("m", k, s, d, p))
            sendMessage (spl nm (s, d) a)
            pauseThread (d + dt)
        )
        sg
    )

st_play_all :: State -> IO State
st_play_all st = do
  play_seq (iseg_nm st) (hr_iot st) (sg_data st) (hr_amp st)
  return st

st_play_marked :: State -> IO State
st_play_marked st = do
  let m = S.elems (sg_mark st)
      sg = map (sg_data st !!) m
  play_seq (iseg_nm st) (hr_iot st) sg (hr_amp st)
  return st

st_print_info :: State -> IO State
st_print_info st = do
  print ("au-fn", au_fn st)
  print ("sg-fn", sg_fn st)
  print ("#-sg", sg_size st, "dur", sum (map (snd . p3_snd) (sg_data st)))
  print ("#-ix", sg_loc st, st_current_annotated_segment st)
  return st

st_merge_marks :: State -> [Int] -> State
st_merge_marks st m =
  let m' = S.fromList m
  in st {sg_mark = S.union m' (sg_mark st)}

st_mark_data :: State -> [Annotated_Segment]
st_mark_data st =
  let d = sg_data st
  in map (d !!) (S.elems (sg_mark st))

st_write_segments :: State -> IO ()
st_write_segments st =
  let ins = intercalate "\t"
      hdr = ins ["#filename", "onset_time", "chunk_length"]
      nm = takeFileName (au_fn st)
      fmt (_, (s, d), _) = printf "%s\t%f\t%f" nm s d
      e = map fmt (st_mark_data st)
  in mapM_ putStrLn (hdr : e)

parse_int :: String -> Maybe Int
parse_int = readMaybe

parse_int_list :: String -> Maybe [Int]
parse_int_list = readMaybe

proc_cmd :: Char -> String -> State -> IO State
proc_cmd c a st =
  case c of
    '?' -> mapM_ print cmd_help >> return st
    'g' -> case parse_int a of
      Nothing -> return st
      Just n -> return (st_set_loc st n)
    'f' -> return (st_relocate st 1)
    'b' -> return (st_relocate st (-1))
    'F' -> return (st_relocate st 10)
    'B' -> return (st_relocate st (-10))
    'z' -> return (st {sg_loc = 0})
    'm' -> return (st {sg_mark = S.insert (sg_loc st) (sg_mark st)})
    'u' -> return (st {sg_mark = S.delete (sg_loc st) (sg_mark st)})
    'M' -> case parse_int_list a of
      Nothing -> return st
      Just m -> return (st_merge_marks st m)
    '>' -> case parse_double a of
      Nothing -> return st
      Just n -> return (st_filter_dur st GT n)
    '<' -> case parse_double a of
      Nothing -> return st
      Just n -> return (st_filter_dur st LT n)
    'i' -> st_print_info st
    'd' -> mapM_ print (sg_data st) >> return st
    'l' -> print (S.elems (sg_mark st)) >> return st
    'L' -> st_write_segments st >> return st
    'p' -> st_play_cur st
    't' -> case parse_double a of
      Nothing -> return st
      Just n -> return (st {hr_iot = n})
    'n' -> proc_cmd 'p' "" st >>= proc_cmd 'f' ""
    'h' -> st_play_marked st
    'H' -> st_play_all st
    'a' -> proc_cmd 'm' "" st >>= proc_cmd 'f' "" >>= proc_cmd 'p' ""
    'r' -> proc_cmd 'u' "" st >>= proc_cmd 'f' "" >>= proc_cmd 'p' ""
    'v' -> return (st {hr_amp = read a})
    'w' -> case a of
      "sin" -> return (st {iseg_nm = "isegS"})
      "rect" -> return (st {iseg_nm = "isegR"})
      _ -> print ("proc_cmd: unknown window", a) >> return st
    'q' -> proc_cmd 'l' "" st >> exitWith ExitSuccess
    _ -> print "proc_cmd: unknown command" >> return st

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  a <- getArgs
  let (use_csv, au, sg) = case a of
        ["-csv", au', sg'] -> (True, au', sg')
        [au', sg'] -> (False, au', sg')
        _ -> error "[-csv] audio-file feature-file"
  au' <- canonicalizePath au
  sg' <- canonicalizePath sg
  print ("loading...", au', sg')
  st <- st_loader (init_st use_csv au' sg')
  let act r = do
        putStr "> "
        l <- getLine
        r' <- case words l of
          [[c0]] -> proc_cmd c0 "" r
          [[c0], a0] -> proc_cmd c0 a0 r
          _ -> print ("act: no command", l) >> return r
        act r'
  forever (act st)
