-- | Meapsoft analysis data input.
module Sound.Analysis.Meapsoft.Data where

import Data.Array.Unboxed {- array -}
import qualified Data.Attoparsec.ByteString.Char8 as P {- attoparsec -}
import qualified Data.Attoparsec.Lazy as PL {- attoparsec -}
import qualified Data.ByteString as B {- bytestring -}
import qualified Data.ByteString.Char8 as C {- bytestring -}
import qualified Data.ByteString.Lazy as L {- bytestring -}
import qualified Data.ByteString.Lex.Fractional as Lex {- bytestring-lexing -}

-- | Read positive and negative @Infinity@, error if string is malformed.
read_inf :: Fractional n => String -> n
read_inf s =
  case s of
    "Infinity" -> 1 / 0
    "-Infinity" -> -1 / 0
    _ -> error ("read_inf: could not read " ++ show s)

read_dec :: C.ByteString -> Maybe (Double, C.ByteString)
read_dec = Lex.readSigned Lex.readDecimal

{- | 'ByteString' to 'Fractional' via 'realToFrac' and 'read_inf'.

>>> to_f (C.pack "-47.56796025464867")
-47.56796025464867

>>> to_f (C.pack "Infinity")
Infinity
-}
to_f :: Fractional n => B.ByteString -> n
to_f s = maybe (read_inf (C.unpack s)) (realToFrac . fst) (read_dec s)

{- | Given the number of columns, reads an entire MEAPsoft data set
  into a list and returns the data paired with the number of rows.
-}
read_data :: Fractional n => FilePath -> Int -> IO (Int, [n])
read_data fn nc = do
  s <- L.readFile fn
  let (Right xs) = decode " \t" s
      xs' = map (take nc . drop 1) (drop 1 xs)
      nr = length xs'
      ds = map to_f (concat xs')
  return (nr, ds)

-- | Class to form 'UArray' given interleaved input data.
class Floating a => Meap_Data a where
  meap_data_uarray :: Int -> Int -> [a] -> UArray (Int, Int) a
  meap_data_index :: UArray (Int, Int) a -> (Int, Int) -> a

instance Meap_Data Float where
  meap_data_uarray nc nr ds = listArray ((0, 0), (nr - 1, nc - 1)) ds
  meap_data_index = (!)

instance Meap_Data Double where
  meap_data_uarray nc nr ds = listArray ((0, 0), (nr - 1, nc - 1)) ds
  meap_data_index = (!)

-- * Delimited-Text

-- | Construct a parser from 'a' terminated by 'b'.
endBy :: P.Parser a -> P.Parser b -> P.Parser a
a `endBy` b = do
  r <- a
  _ <- b
  return r

-- | A lazy 'ByteString' parser for delimited text.
parser :: String -> P.Parser [[C.ByteString]]
parser delims = line `P.manyTill` P.endOfInput
 where
  line = (field `P.sepBy` sep) `endBy` eol
  field = P.takeWhile (P.notInClass (delims ++ nls))
  sep = P.satisfy (P.inClass delims)
  eol = P.skipMany1 (P.satisfy $ P.inClass nls)
  nls = "\n\r"

{- | Parse records separated by newlines from a ByteString.
Record fields are separated by any of the characters in 'delims'. There is
no way of escaping delimiters, so record fields may not contain any of the
characters in 'delims'.
-}
decode :: String -> L.ByteString -> Either String [[C.ByteString]]
decode delims = PL.eitherResult . PL.parse (parser delims)
