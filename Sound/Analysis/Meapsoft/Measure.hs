-- | Functions related to meapsoft analysis measures.
module Sound.Analysis.Meapsoft.Measure where

-- | Cycles per second to Mel conversion, see Stevens & Volkman, 1940.
cps_mel :: Floating n => n -> n
cps_mel f = 1127.01048 * log (f / 700 + 1)

-- | Mel to cycles per second conversion, see Stevens & Volkman, 1940.
mel_cps :: Floating n => n -> n
mel_cps m = 700 * (exp (m / 1127.01048) - 1)

-- | Cycles per second to Erb conversion, see Glasberg & Moore, 1990, p. 114.
cps_erb :: Floating n => n -> n
cps_erb f = 21.4 * logBase 10 (0.00437 * f + 1)
