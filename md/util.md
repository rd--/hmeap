# browse

Interactively examine MEAP file.

# convert

Conversion from a list of onset times to a hmeap `segmentation` file.

~~~~
$ echo 1 2 3 5 8 13 21 34 > /tmp/t.onsets
$ hmeap-convert onsets /tmp/t.onsets
#filename       onset_time      chunk_length
/tmp/t  1.0     1.0
/tmp/t  2.0     1.0
/tmp/t  3.0     2.0
/tmp/t  5.0     3.0
/tmp/t  8.0     5.0
/tmp/t  13.0    8.0
/tmp/t  21.0    13.0
$
~~~~

# plot

Use `hmatrix` and `gnuplot` bindings to plot unit dimensioned features.

# print-header

Read & print header of MEAP file as CSV data.

~~~~
$ hmeap-print-header cm.feat
Feature Name     ,Column,Degree
onset_time       ,0     ,1
chunk_length     ,1     ,1
AvgChroma        ,2     ,12
AvgChromaScalar  ,14    ,1
AvgChunkPower    ,15    ,1
AvgFreqSimple    ,16    ,1
AvgMelSpec       ,17    ,40
AvgMFCC          ,57    ,13
AvgPitch         ,70    ,1
AvgSpec          ,71    ,513
AvgSpecCentroid  ,584   ,1
AvgSpecFlatness  ,585   ,1
AvgTonalCentroid ,586   ,6
ChunkLength      ,592   ,1
ChunkStartTime   ,593   ,1
Entropy          ,594   ,1
RMSAmplitude     ,595   ,1
SpectralStability,596   ,1
$
~~~~

# segments

Print segmentation as csv file, ie. "start-time,duration"

# stat

Use hmatrix `GSL` bindings to do basic statistical analysis.
