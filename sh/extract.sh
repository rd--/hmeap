#!/bin/bash

if test $# != 3
then
    echo "extract.sh MEAPsoft.jar audio-file threshold"
    exit 0
fi

mp_fn=$1
au_fn=$2
sg_fn=${au_fn%.*}.T$3.seg
ft_fn=${au_fn%.*}.T$3.feat

if test ! -f $mp_fn
then
  echo "non-existant MEAPsoft.jar: " $mp_fn
  exit 1
fi

echo "file to analyse: " $au_fn $sg_fn "(" $ft_fn ")"

# -Xms = initial heap size
# -Xmx = maximum heap size

if test -f $sg_fn
then
    java \
        -Xms128m \
        -Xmx512m \
        -cp $mp_fn \
        com.meapsoft.FeatExtractor \
        -f AvgChroma \
        -f AvgChromaScalar \
        -f AvgChunkPower \
        -f AvgFreqSimple \
        -f AvgMelSpec \
        -f AvgMFCC \
        -f AvgPitch \
        -f AvgSpec \
        -f AvgSpecCentroid \
        -f AvgSpecFlatness \
        -f AvgTonalCentroid \
        -f ChunkLength \
        -f ChunkStartTime \
        -f Entropy \
        -f RMSAmplitude \
        -f SpectralStability \
        $sg_fn \
        -o $ft_fn
else
    echo "$sg_fn does not exist"
fi
