#!/bin/bash

if test $# != 3
then
    echo "segment.sh MEAPsoft.jar audio-file threshold"
    exit 0
fi

mp_fn=$1
au_fn=$2
sg_fn=${au_fn%.*}.T$3.seg

if test ! -f $mp_fn
then
  echo "non-existant MEAPsoft.jar: " $mp_fn
  exit 1
fi

echo "file to analyse: " $au_fn "(" $sg_fn ")"
echo "threshold: " $3

# -t = threshold (0.0 -> 3.0), lower values are more sensitive
# -s = smoothing window (seconds)
# -d = event detector (not beat detector)

# -Xms = initial heap size
# -Xmx = maximum heap size

if test -f $sg_fn
then
    echo "$sg_fn exists"
else
    java \
        -cp $mp_fn \
        com.meapsoft.Segmenter \
        -o $sg_fn \
        $au_fn \
        -t $3 \
        -s 0.1 \
        -d
fi
