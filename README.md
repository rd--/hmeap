hmeap
-----

[Haskell](http://haskell.org/) parser for
[Meapsoft](https://www.meapsoft.org/) 2.0
analysis files

Supports analysis files with any number of features extracted.

Includes programs that illustrate using hmeap with
hmatrix
[gnuplot](http://www.gnuplot.info/) and
[hsc3](http://rohandrape.net/?t=hsc3)

## cli

[mark](?t=hmeap&e=md/mark.md),
[play](?t=hmeap&e=md/play.md),
[util](?t=hmeap&e=md/util.md)

© [rohan drape](http://rohandrape.net/) and others,
  2007-2023,
  [gpl](http://gnu.org/copyleft/)

with contributions by:

- [stefan kersten](http://space.k-hornz.de/)

see the [history](?t=hmeap&q=history) for details
